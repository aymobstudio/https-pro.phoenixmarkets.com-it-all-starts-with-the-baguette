<?php require_once 'geoip.php'; ?>
<?php require_once "form_new.php"; ?>
<!DOCTYPE html>
<html>

<head>
    <title>Revealed Amazon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--font-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300;400;700&display=swap" rel="stylesheet">    
    <link href="https://fonts.googleapis.com/css2?family=Magra:wght@400;700&family=Open+Sans&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@700&display=swap" rel="stylesheet">  
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400&display=swap" rel="stylesheet">
     <!--font-->

    <!-- INTEL -->
    <link rel="stylesheet" href="intl-tel-input-master/intl-tel-input-master/build/css/intlTelInput.css">
    <!-- INTEL -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K2HXPSJ');</script>
<!-- End Google Tag Manager -->
</head>
    <body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src=""https://www.googletagmanager.com/ns.html?id=GTM-K2HXPSJ""
height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="bg-img">
        <div class="container">
            <div class="row bg-text-top">
                <div class="col-lg-7 padding">
                    <div class="hero-heading-div">
                        <div class="hero-heading1">Advertorial</div>
                        <div class="hero-heading2">
                            <div >HOW TO INVEST IN</div>
                        </div>
                        
                        <div class="hero-heading3">amazon</div>
                        <div class="hero-heading4">CFDs</div>
                    </div>
                    
                </div>
                <div class="col-lg-5 padding">
                    
                    <div class="hero-form-bg">
                        <div class="hero-form-heading">
                        Now is the time to invest in amazon CFDs</div>
                            <div class="hero-form">
                        <form class="form-inline" method="post">
                            <input type="hidden" id="country" name="country"  value="<?= $geo['country']; ?>">
                            <input type="hidden" id="citizen" name="citizenship"  value="<?= $geo['citizenship']; ?>">
                            <input type="hidden" id="userip" name="userip"  value="<?= $geo['ip']; ?>">
                            <input type="hidden" id="fullUrl" name="fullUrl"  value="">
                            <input type="text" class=" input-field  " placeholder="First Name" name="first_name" value="<?= (!empty($data['first_name'])) ? $data['first_name'] : ''; ?>" required>
                           
                            <input type="text" class=" input-field form-imput-margin" placeholder="Last Name" name="last_name" value="<?= (!empty($data['last_name'])) ? $data['last_name'] : ''; ?>" required> 
                       
                            <small class="text-danger font-weight-bold"><?= (!empty($data['first_name_err'])) ? $data['first_name_err'] : ''; ?></small>
                            <small class="text-danger font-weight-bold"><?= (!empty($data['last_name_err'])) ? $data['last_name_err'] : ''; ?></small>  
                            <div class="input-group email-margin ">
                                <input type="email" class="form-control input-field  " placeholder="E-Mail Address" id="demo" name="email" value="<?= (!empty($data['email'])) ? $data['email'] : ''; ?>" required>
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="fa fa-envelope-o"></i></span>
                                </div>
                              </div>
                              <small class="text-danger font-weight-bold"><?= (!empty($data['email_err'])) ? $data['email_err'] : ''; ?></small>
                              <div class="input-group email-margin ">
                                <input type="password" class="form-control input-field  " placeholder="Password" id="password" name="password">
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="fa fa-unlock"></i></span>
                                </div>
                              </div>
                              <small class="text-danger font-weight-bold"><?= (!empty($data['password_err'])) ? $data['password_err'] : ''; ?></small>
                              <div class="w-100">
                                <div class="form-group email-margin">
                                    <div class="input-group ">
                                        <input name="phone" type="text" class="mr-auto form-control phone-field "  id="phone" aria-describedby="basic-addon2">
                                       
                                        <div class="input-group-append">
                                          <span class="input-group-text" id="basic-addon2"><i class="fa fa-phone "></i></span>
                                        </div>
                                      </div> 
                                      <span id="valid-msg" class="hide"></span>
                                        <span id="error-msg" class="hide text-danger"></span>                                 
                                  </div> 
                                           
                              </div>
                              <div class="check-input">
                                <input  type="checkbox" name="check" id="check" >
                                <label class="check-text">	I accept the Terms & Conditions I confirm that I am 18 or older </label><br>
                                </div>
                                <button class="form-btn" disabled="disabled" id="submit" type="submit" name="submit" style="background:#000; color:#fff">JETZT ANFANGEN</button>
               
                          
                            <p class="form-p">SMS verification required. Minimum investment required is €250.</p>
                             <div class="pay-btn"><img src="./image/icons.png"></div>
                          </div>
                          </form>
                    </div>
                </div>

                

            </div>
        </div>

    </div>
    <div class="yellow-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                   
                    <div class="span-number">1</div>
                    <div class="yellow-p yellow-mt">Fill out the form</div>
                
                </div>
                <div class="col-lg-4">
                    
                    <div class="span-number">2</div>
                    <div class="yellow-p">
                    Complete your account verification with our authorized representative</div>
                    
                </div>
                <div class="col-lg-4">
                    <div class="span-number">3</div>
                    <div class="yellow-p yellow-mt-last">
                    Start investing in your financial future </div>
                </div>

            </div>

        </div>

    </div>
    <div class="container">
        <div class="main-heading">
            <h2>Amazon's shares forecast to rise 119.37%. Here’s 5 pros to buying Amazon stock.</h2>
        </div>
        <div class="border-div">
            <h3 class="border-heading">Key Points</h3>
            <div class="border-text">
                <ul>
                <li><em>Amazon’s share is over 450% higher than it was five years ago, more than six times the S&P 500 index’s 67% return.</em></li>
                <li><em>Amazon stock could reach a price of €10,720 by mid-November 2024.</em></li>
                <li><em>The stock has also climbed a staggering 2,220% in the past decade.</em></li>
                </ul>
                </div>

        </div>
        <div class="row main-body-padding">
            <div class="col-lg-6  left-right-padding">
                <div  class="left-side-text">
                    A major advantage in owning Amazon stock is the company’s willingness to disrupt a wide range of industries.
                    <br><br>
                    Mid to long term investing can be life changing when you buy and hold the truly great businesses. While not every stock performs well, when investors win, they can win big.
                    <br><br>
                    Don’t believe it? Then look at the Amazon.com, Inc. (NASDAQ:AMZN) share price. Continuing to clobber the market, at the time of writing, it’s over 450% higher than it was five years ago, more than six times the S&P 500 index’s 67% return. The stock has also climbed a staggering 2,220% in the past decade.
                    <br><br>
                    With the continuing growth of online retail and ever-booming ecommerce sector, AMZN stock has advanced so quickly in recent years that the company is worth nearly €1 trillion.
                    <br><br>
                    <b>1. Exceptional Leadership</b>
                    <br>Like other ridiculously successful Silicon Valley companies Facebook (FB), Netflix (NFLX) and Alphabet (GOOGL), Amazon is a founder-run company. But even compared to the enviable track records of those names, Jeff Bezos’ leadership is exceptional.
                    <br><br>
                    Bezos’ obsession with customer service is now a core part of Amazon’s ethos – as is his ruthlessness. And if there were any doubt, Bezos is also the largest single owner of Amazon stock (12%), so his interests are clearly aligned with shareholders.
                    <br><br>
                    Bezos didn’t become the world’s richest person by accident – he’s the single best allocator of capital in the world right now, which means he’s seldom unprepared for future opportunities and challenges.
                    <br><br>
                    <b>2. Amazon Continues to Disrupt Multiple Industries</b>
                    <br>
                    Another advantage to owning Amazon stock is, simply put, the company’s willingness, desire and ability to disrupt an extraordinary range of industries.
                    <br><br>
                    Amazon.com started as a disruptor, forcing brick-and-mortar booksellers out of business due to its scale, lower overhead and convenience. It didn’t take long for Amazon to push from hawking books to all sorts of product categories – toys, electronics, clothes, tools, you name it.
                    <br><br>
                    The e-commerce giant’s ambitions continued to evolve, and in 2017 it surprised Wall Street by acquiring organic foods grocer Whole Foods for €13.4 billion. It even offers its own branded credit card, is building out its own delivery service and is considering moving seriously into retail banking.
                    <br><br>
                    </div>
                    <button type="button" class="amazon-btn yellow-button btn btn-primary">Discover how to benefit from investing in Amazon</button>
                        <div class="left-side-text">
                            <b>3. Amazon Still Has A Massive Addressable Market</b>
                            <br>
                            Amazon’s total addressable market (TAM) is the largest among all top internet tech platforms. It’s estimated that Amazon’s e-commerce platform has a €20 trillion TAM and is only about 10% penetrated. Amazon Cloud Services (AWS), the company’s most profitable segment, has an additional €1 trillion TAM and is also only about 10% penetrated.
                            <br><br>
                            In addition, Amazon has only reached about 30% penetration into a €1 trillion potential advertising opportunity and 10% penetration into a €5 trillion business supplies market.
                            <br><br>
                            <b>4. Amazon Is Outcompeting Its Rivals</b>
                            <br>
                            With more than 100 million loyal Prime subscribers, more than half of all U.S. shoppers now start their search on Amazon. Amazon has about 40% share of the U.S. e-commerce market, but that share is still growing. As Amazon invests in improving convenience, selection and pricing, it could and probably will be extremely difficult for competitors to keep pace.
                            <br><br>
                            <b>5. Amazon Generates Consistent Growth</b>
                            <br>
                            While Amazon may not be generating the 40%-plus revenue growth it once was, the fact that only two of Amazon’s past 66 quarters have produced organic revenue growth of less than 20% is incredibly impressive. Amazon’s profitability has been inconsistent due to heavy growth investment, but it has also trended consistently positively over time, averaging 21.5% revenue growth over the past four quarters.
                            <br><br>
                            <b>Amazon Stock Price Set to Skyrocket?</b>
                            <br>
                            Online forecasting service Longforecast.com, predicts that the company’s stock will experience a long-term increase, with the Amazon shares trading at €2,398 by November 2021. Another forecasting service Wallet Investor has also taken a bullish stance, expecting Amazon stock to trade at €3,841 by November 2024, meaning that an investment today, would see revenue at around +119.37 percent in five years.
                            <br><br>
                            Gov Capital offers the most optimistic outlook of all, stating that Amazon stock could reach a price of €10,720 by mid-November 2024.
                            <br><br>
                            <b>The Bottom Line</b>
                            <br>
                            So, should you buy Amazon stock before this year comes to an end? Like any other form of investment, there is no guarantee of success. Nevertheless, according to Amazon’s stock trend analysis and forecasts, there’s more than a good chance that the company could bring substantial growth to your portfolio in the next two years.
                            <br>
                            </div>

            </div>


            <div class="col-lg-6 left-right-padding">
                <a href="#" class="amazon-btn"><img class="forecast-img" src="./image/forecast-1.jpg"></a>
                <div class="forecast-text">
                Continuing to clobber the market, at the time of writing, it's over 450% higher than it was five years ago, more than six times the S&P 500 index's 67% return.</div>
                    <a href="#" class="amazon-btn"><img class="forecast-img" src="./image/forecast-2.jpg"></a>
                    <div class="forecast-text">
                    Gov Capital offers the most optimistic outlook of all, stating that Amazon stock could reach a price of €10,720 by mid-November 2024.
                    </div>
                    <a href="#" class="amazon-btn"><img class="forecast-img" src="./image/forecast-3.jpg"></a>
                    <div class="forecast-text">
                    According to Amazon’s stock trend analysis and forecasts, there’s more than a good chance that the company could bring substantial growth to your portfolio in the next two years.
                    </div>
                    <button type="button" class="amazon-btn yellow-button btn btn-primary">Discover how to benefit from investing in Amazon</button>
            </div>

        </div>
    </div>
    <!--footer-->
    <div class="footer-bg">
        <div class="footer-container">
            <!-- <a href="#"><img class="footer-logo" src="./image/investingfinancial-logo.png"></a> -->
            <div class="bottom-text">
                <p>HIGH RISK INVESTMENT WARNING: Investments are complex instruments and come with a high risk of losing money rapidly due to leverage. 82% of retail investor accounts lose money when trading Investments with this provider. You should consider whether you understand how Investments work and whether you can afford to take the high risk of losing your money. Click here to read our Risks Disclosure. Phoenix Markets is the trading name of WGM Services Ltd, a financial services company subject to the regulation and supervision of the Cyprus Securities Exchange Commission (CySEC) under license number 203/13. The offices of WGM Services Ltd are located at 11, Vizantiou, 4th Floor, Strovolos 2064, Nicosia, Cyprus.</p>
                <p>The information and services contained on this website are not intended for residents of the United States or Canada.</p>
            </div>
            <div class="footer-link-div">
                <div class="a-div">
                    <a target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/privacy_policy.pdf" class="a-tag">
                        Privacy Policy</a>
                </div>
                <div class="a-div">
                    <a target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/terms_and_conditions.pdf" class="a-tag">                        
                        Terms and Conditions</a>
                </div>
                <div class="a-div">
                    <a target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/cookies_policy.pdf" class="a-tag">                        
                        Cookie Policy</a>
                </div>
            </div>

        </div>
    </div>
    <script src="intl-tel-input-master/intl-tel-input-master/build/js/intlTelInput.js"></script>
    <script>
    /* scroll to div */
        $(".amazon-btn").click(function() {
            $('html, body').animate({
                scrollTop: $(".hero-form-bg").offset().top
            }, 2000);
        });
        var initialCountryValue = $('#country').val();
        $( document ).ready(function() {
            $("#fullUrl").val($(location).attr('href'));
        });
        
        // Initialize int tel input
        var input = document.querySelector("#phone"),
        errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg");

        // here, the index maps to the error code returned from getValidationError - see readme
        var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

        // initialise plugin
        var iti = window.intlTelInput(input, {
            initialCountry: initialCountryValue,
            nationalMode: true,
            separateDialCode: true,
            hiddenInput: "full_phone",
            utilsScript: "intl-tel-input-master/intl-tel-input-master/build/js/utils.js?1603274336113"
        });

        var reset = function() {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
        };

        // on blur: validate
        input.addEventListener('blur', function() {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
            validMsg.classList.remove("hide");
            } else {
            input.classList.add("error");
            var errorCode = iti.getValidationError();
            errorMsg.innerHTML = errorMap[errorCode];
            errorMsg.classList.remove("hide");
            }
        }
        });

        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);



        // Disable submit button untill input check is checked
        $(document).ready(function() {
            var the_terms = $("#check");
            the_terms.click(function() {
                if ($(this).is(":checked")) {
                    $("#submit").removeAttr("disabled");
                } else {
                    $("#submit").attr("disabled", "disabled");
                }
            });
        }); 
      </script>
</body>
</html>










