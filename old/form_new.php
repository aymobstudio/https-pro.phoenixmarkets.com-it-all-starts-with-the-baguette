<?php

require_once "functions/register.php";
require_once "functions/autoLogin.php";
require_once "functions/newLeadEmail.php";
require_once "functions/errorEmail.php";

require_once "vendor/autoload.php";

$funnelName = 'AMZDE';
$description = 'AMAZON!!! das is good (in German)';

$error = '';
$data = [
    'first_name' => '',
    'last_name' => '',
    'email' => '',
    'password' => '',
    'phone' => '',
    'country' => '',
    'citizenship' => '',
    'currency' => '',
    'city' => '',
    'postalCode' => '',
    'birthday' => '',
    'first_name_err' => '',
    'last_name_err' => '',
    'email_err' => '',
    'password_err' => '',
    'phone_err' => '',
];


if($_SERVER['REQUEST_METHOD'] === 'POST'){
  $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
 
  $data = [
    'first_name' => trim($_POST['first_name']),
    'last_name' => trim($_POST['last_name']),
    'email' => trim($_POST['email']),
    'password' => trim($_POST['password']),
    'phone' => str_replace("+", "", $_POST['phone']),
    'full_phone' => $_POST['full_phone'],
    'userip' => $_POST['userip'],
    'first_name_err' => '',
    'last_name_err' => '',
    'email_err' => '',
    'password_err' => '',
    'phone_err' => '',
    'pass_length' => 8
  ];
  $referral = '';
    if(isset($_GET['clickid'])){
        $clickid = $_GET['clickid'];
    }


    // validate firstname field
    if(1 === preg_match('~[0-9]~', $data['first_name'])){
        $data['first_name_err'] = 'Name shouldn\'t contain numbers!';
    } elseif(empty($data['first_name'])){
        $data['first_name_err'] = 'Field is empty!';
        //$data['first_name_err'] = 'asdf';
    }
    // validate lastname field
    if(1 === preg_match('~[0-9]~', $data['last_name'])){
        $data['last_name_err'] = 'Last Name shouldn\'t contain numbers!';
    } elseif($data['last_name'] == ''){
        $data['last_name_err'] = 'Field is empty!';
    }
    //validate email field
    if(filter_var($data['email'], FILTER_VALIDATE_EMAIL) == true){
        $data['email_err'] == '';
    } else {
        $data['email_err'] = 'Invalid Email!';
    }
    //validate password field
    if(empty($data['password'])){
        $data['password_err'] = 'Password field is empty!';
    } elseif(!preg_match('/^\p{Xan}+$/', $data['password'])){
        $data['password_err'] = 'Password should contain only letters and numbers!';
    } elseif(strlen($data['password']) < $data['pass_length']){
        $data['password_err'] = 'Password must be at least '.$data['pass_length'].' characters long!';
    } elseif(!preg_match("#[0-9]+#", $data["password"])){
        $data['password_err'] = 'Password should contain at least one number!';
    } elseif( !preg_match("#[a-zA-Z]+#", $data["password"])){
        $data['password_err'] = 'Password should contain at least one letter!';
    } elseif( !preg_match("#[A-Z]+#", $data["password"])){
        $data['password_err'] = 'Das Passwort sollte mindestens einen Großbuchstaben enthalten!';
    } 
    // validate phone field
    if($data['phone'] == ''){
        $data['phone_err'] = 'Phone number field is empty!';
    }

    // if error variables empty proceed with execution
    if(empty($data['first_name_err']) && empty($data['last_name_err']) && empty($data['email_err']) && empty($data['phone_err']) && empty($data['password_err'])){
      // create new customer function call
      $register = createNewCustomer($data['userip'], $data['first_name'], $data['last_name'], $data['email'], $data['password'], $data['full_phone'], $funnelName, $clickid, $description);
      sendNewLeadEmail($funnelName, $description, $data['first_name'], $data['last_name'], $data['email'], $data['full_phone'], $referral, $clickid);
    //   session_start();
    //   $_SESSION['loginUrl'] = $register['addonData']['data']['loginURL'];
      $submitted = 'submitted';
      
      header("Location: thankyou.php?".$clickid);
    }
}