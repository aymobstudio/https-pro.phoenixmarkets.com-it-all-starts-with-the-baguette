<!DOCTYPE html>
<html>

<head>
    <title>Thank You</title>
    <link rel="stylesheet" href="css/thankyou.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K2HXPSJ');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src=""https://www.googletagmanager.com/ns.html?id=GTM-K2HXPSJ""
height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div id="content">
        <div class="left_content fl">

            <div class="heading_div">
                <h1>Vielen Dank für Ihre Anfrage</h1>
                <span>Sie sind der Verbesserung Ihrer finanziellen Zukunft einen Schritt näher gekommen!<br/>So geht es weiter… </span>   

            </div>
            <div class="three_div_container">
                <div class="first_div full_div fl">
                    <div class="topheader first_header">
                        <div class="number_top">1</div>

                    </div>
                    <div class="mid first_header">
                        <h3>Wir übernehmen</h3>
                        Unser freundliches Team wird Ihre Anfrage an den am besten geeigneten Berater in Ihrer Nähe weiterleiten
                    </div>
                </div>

                <div class="mid_div full_div fl">
                    <div class="topheader mid_header">
                        <div class="number_top">2

                        </div>

                    </div>
                    <div class="mid first_header line_spacing">
                        <h2>Erwarten Sie einen Anruf</h2>
                        Wir senden Ihnen eine SMS und eine E-Mail-Bestätigung sowie einen Anruf von unseren kompetenten Partnern bei Financiable
                    </div>
                </div>

                <div class="last_div full_div fl">
                    <div class="topheader last_header">
                        <div class="number_top">3</div>

                    </div>
                    <div class="mid first_header">
                        <h3>Sie können sich entspannen</h3>
                        Seelenfrieden durch die Gewissheit, dass Ihre Finanzen professionell bewertet worden sind
                    </div>
                </div>

                <div class="clear"></div>


            </div>
        </div>
        <div class="right_content fr">
            <div class="right_top_content">
                <div class="top_shild">
                    <img src="image/thankyou/animation_200_kssitijb.gif">
                </div>
                <div class="top_content">
                    <h3>Was Sie Wissen Sollten</h3>
                    <div class="content_box">
                       <div class="check fl"><img src="image/thankyou/check.png"></div> 
                       <div class="check_content fl">Wir werden Sie NICHT nach sensiblen persönlichen Daten wie Ihrem Geburtsdatum oder Ihrer Bankverbindung fragen.</div> 
                           <div class="clear"></div>
                    </div>
                    <div class="content_box">
                        <div class="check fl"><img src="image/thankyou/check.png"></div> 
                        <div class="check_content fl">Ihre Kontaktdaten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.</div> 
                            <div class="clear"></div>
                     </div>
                     <div class="content_box">
                        <div class="check fl"><img src="image/thankyou/check.png"></div> 
                        <div class="check_content fl"> Alle Berater in unserem Gremium sind von der Financial Conduct Authority anerkannt und reguliert.</div> 
                            <div class="clear"></div>
                     </div>

                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="background"></div>


</body>

</html>