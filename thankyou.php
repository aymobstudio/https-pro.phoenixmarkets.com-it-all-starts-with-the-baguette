<!DOCTYPE html>
<html>

<head>
    <title>Thank You</title>
    <link rel="stylesheet" href="css/thankyou.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K2HXPSJ');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src=""https://www.googletagmanager.com/ns.html?id=GTM-K2HXPSJ""
height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div id="content">
        <div class="left_content fl">

            <div class="heading_div">
                <h1>Thank you for your inquiry</h1>
                <span>You are one step closer to improving your financial future!<br/>This is what happens next... </span>   

            </div>
            <div class="three_div_container">
                <div class="first_div full_div fl">
                    <div class="topheader first_header">
                        <div class="number_top">1</div>

                    </div>
                    <div class="mid first_header">
                        <h3>We Take Over</h3>
                        Our friendly team will match your enquiry to the most suitable advisor near you
                    </div>
                </div>

                <div class="mid_div full_div fl">
                    <div class="topheader mid_header">
                        <div class="number_top">2

                        </div>

                    </div>
                    <div class="mid first_header line_spacing">
                        <h2>Expect a Call</h2>
                        We’ll send you a text and email confirmation plus a call from our expert partners
                    </div>
                </div>

                <div class="last_div full_div fl">
                    <div class="topheader last_header">
                        <div class="number_top">3</div>

                    </div>
                    <div class="mid first_header">
                        <h3>You Can Relax</h3>
                        Peace of mind from knowing your finances have been professionally assessed
                    </div>
                </div>

                <div class="clear"></div>


            </div>
        </div>
        <div class="right_content fr">
            <div class="right_top_content">
                <div class="top_shild">
                    <img src="image/thankyou/animation_200_kssitijb.gif">
                </div>
                <div class="top_content">
                    <h3>What You Should Know</h3>
                    <div class="content_box">
                       <div class="check fl"><img src="image/thankyou/check.png"></div> 
                       <div class="check_content fl">
                        We will NOT ask you for any sensitive personal details such as date of birth or your bank account information.</div> 
                           <div class="clear"></div>
                    </div>
                    <div class="content_box">
                        <div class="check fl"><img src="image/thankyou/check.png"></div> 
                        <div class="check_content fl">Your contact details will not be shared with any third parties without your express permission.</div> 
                            <div class="clear"></div>
                     </div>
                     <div class="content_box">
                        <div class="check fl"><img src="image/thankyou/check.png"></div> 
                        <div class="check_content fl"> All advisors on our panel are recognised and regulated by the Financial Conduct Authority.</div> 
                            <div class="clear"></div>
                     </div>

                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="background"></div>


</body>

</html>