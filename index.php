<!DOCTYPE html>
<html>

<head>
    <title>It all starts with a baguette</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--font-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Magra:wght@400;700&family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <!-- INTEL -->
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K2HXPSJ');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K2HXPSJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="bg-img">
        <div class="container">
            <div class="row bg-text-top">
                <div class="col-lg-7 padding">
                    <div class="hero-heading-div">
                        <div class="hero-heading1">Advertorial</div>
                        <div class="hero-heading2">
                            <div>It all starts with a baguette</div>
                        </div>

                        <!-- <div class="hero-heading3">amazon</div> -->
                        <div class="hero-heading4">	Prepare for the Coming Price Hikes and Hedge Your Investments</div>
                    </div>

                </div>
                <div class="col-lg-5 padding">

                    <div class="hero-form-bg">
                        <div class="hero-form-heading">
                            Start Trading Today</div>
                        <div class="hero-form">
                            <form class="" action="submit.php" method="post" onsubmit="return Validation()">

                                <?php if (isset($_GET['clickid']) && ($_GET['clickid'] != "")) { ?>
                                    <input type="hidden" value="<?php echo $_GET['clickid']; ?>" name="clickid" />
                                <?php  } ?>
                                <input type="hidden" value="" name="title" id="title" />

                                <div class="input-group email-margin ">
                                    <input type="text" class="form-control input-field  " placeholder="Full Name" name="FirstNameLastName" id="fname" onkeyup="checkName();" required>
                                    <div id="fname-message" class="error-msg"></div>
                                </div>

                                <div class="input-group email-margin ">
                                    <input type="email" class="form-control input-field  " placeholder="E-Mail Address" id="email" name="email" onkeyup="checkEmail();" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fa fa-envelope-o"></i></span>
                                    </div>
                                    <div id="email-message" class="error-msg"></div>
                                </div>

                                <div class="input-group email-margin ">
                                <input type="password" class="form-control input-field" placeholder="Password" id="password" name="password" onkeyup="checkPassword();" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fa fa-unlock"></i></span>                                    
                                    </div>
                                    <div id="password-message" class="error-msg"></div>
                                </div>

                                <div class="w-100">
                                    <div class="form-group email-margin">
                                        <input class="form-group input-field " style="width: 100% !important;" type="tel" name="phone" id="phone" class="phone form-control input-fields" placeholder="Phone Number" required="true" value="">
                                        <div id="phone-message" class="error-msg"></div>
                                    </div>
                                </div>

                                <div class="check-input mt-3">
                                    <input type="checkbox" name="check" id="check" checked required>
                                    <label class="check-text"> I accept the Terms & Conditions I confirm that I am 18 or older </label><br>
                                </div>
                                <button class="form-btn" id="commence-btn" type="submit" name="submit" style="background:#000; color:#fff">Start Investing</button>


                                <!-- <p class="form-p">SMS verification required. Minimum investment required is €250.</p> -->
                                <div class="pay-btn"><img src="./image/icons.png"></div>
                        </div>
                        </form>
                    </div>
                </div>



            </div>
        </div>

    </div>
    <div class="yellow-section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p>
                        	
Think of the average baker in Paris who makes a living from selling baguettes and baked breads. The same baker relies on Russia’s wheat shipment to France to attain the wheat for producing 100% of the flour-based products sold in the bakery. Russia fails to harvest wheat this year, causing scarcity in the market. 
                    </p>
                </div>
                <!-- <div class="col-lg-4">

                    <div class="span-number">1</div>
                    <div class="yellow-p yellow-mt">Fill out the form</div>

                </div>
                <div class="col-lg-4">

                    <div class="span-number">2</div>
                    <div class="yellow-p">
                        Complete your account verification with our authorized representative</div>

                </div>
                <div class="col-lg-4">
                    <div class="span-number">3</div>
                    <div class="yellow-p yellow-mt-last">
                        Start investing in your financial future </div>
                </div> -->

            </div>

        </div>

    </div>
    <div class="container">
        <!-- <div class="main-heading">
            <h2>Amazon's shares forecast to rise 119.37%. Here’s 5 pros to buying Amazon stock.</h2>
        </div> -->
        <div class="border-div">
            <h3 class="border-heading">Key Points</h3>
            <div class="border-text">
            In turn, Russia increases its export tax, upping the price of wheat in 30%. The same baker will now need to increase the bakery’s revenue by 30% just to break even from last year’s revenue on the same exact products sold. In turn, customers will be forced to pay a few more cents per baguette, causing an increase in the average French consumer’s cost of living. The same average consumer may have a business of his own. As the cost of living increases, business owners from all sectors, not just those in the food industry, are urged to increase their prices in response to market prices.
                <!-- <ul>
                    <li><em>Amazon’s share is over 450% higher than it was five years ago, more than six times the S&P 500 index’s 67% return.</em></li>
                    <li><em>Amazon stock could reach a price of €10,720 by mid-November 2024.</em></li>
                    <li><em>The stock has also climbed a staggering 2,220% in the past decade.</em></li>
                </ul> -->
            </div> 
         </div>

        <div class="row main-body-padding">
            <div class="col-lg-6  left-right-padding">
                <div class="left-side-text">
                The ripple effect spreads not just through the borders of France, but to all of Europe and the world alike. What begins as a difference of just 5 or 10 cents extra per day for a simple baguette quickly adds to food inflation which affects world inflation.

When basic ingredients for food increase in price, populations are less likely to spend their hard-earned paychecks on luxuries – consequently less money circling in expanding markets. As the pattern develops globally, possible threats at government recessions are posed.
                    <br><br>
                    <b>When Inflation Rises, So Do Investors</b>
                    <br>
                    The level of inflation in an economy varies depending on current events. A rapid increase in raw materials, such as wheat, contribute to the value of the US dollar.
                    As a result of the increase on the price of wheat and other core products, the US Dollar and other currencies alike will have decreased purchasing power, allowing spenders able to buy less with their money’s worth. Investors, on the other hand, are using the proper investment strategies to hedge against inflation and increase their capital as a result of inflation.
                    <br><br>
                </div>
                <button type="button" class="amazon-btn yellow-button btn btn-primary">
                    Prepare for Price Hikes and Hedge Your Investments
                </button>
                <div class="left-side-text">
                <b>What are investors doing to profit from inflation?</b>
                <br>
                Disciplined investors can plan for inflation by investing in asset classes that outperform the market during inflationary climates. Times like these when inflation is on the brink, market investors are profiting significantly by striking anti-inflation commodities like gold, precious metals, wheat, and other assets like cryptocurrencies to help their portfolios thrive when inflation hits.
                </div>

            </div>


            <div class="col-lg-6 left-right-padding">
                <!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/EURUSD/?exchange=FX" rel="noopener" target="_blank"><span class="blue-text">EURUSD Rates</span></a> by TradingView</div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js" async>
  {
  "symbol": "FX:EURUSD",
  "width": 350,
  "height": 220,
  "locale": "en",
  "dateRange": "12M",
  "colorTheme": "light",
  "trendLineColor": "rgba(41, 98, 255, 1)",
  "underLineColor": "rgba(41, 98, 255, 0.3)",
  "underLineBottomColor": "rgba(41, 98, 255, 0)",
  "isTransparent": false,
  "autosize": false,
  "largeChartUrl": ""
}
  </script>
</div>
<!-- TradingView Widget END -->

                <div class="forecast-text">
                    Continuing to clobber the market, at the time of writing, it's over 450% higher than it was five years ago, more than six times the S&P 500 index's 67% return.</div>
                <a href="#" class="amazon-btn"><img class="forecast-img" src="./image/forecast-2.jpg"></a>
                <div class="forecast-text">
                A rapid increase in raw materials, such as wheat, contribute to the value of the US dollar.
                </div>
                
                <!-- TradingView Widget BEGIN -->
                    <div class="tradingview-widget-container">
                    <div class="tradingview-widget-container__widget"></div>
                    <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/currencies/forex-cross-rates/" rel="noopener" target="_blank"><span class="blue-text">Exchange Rates</span></a> by TradingView</div>
                    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-forex-cross-rates.js" async>
                    {
                    "width": 770,
                    "height": 400,
                    "currencies": [
                        "EUR",
                        "USD",
                        "JPY",
                        "GBP",
                        "CHF",
                        "AUD",
                        "CAD",
                        "NZD",
                        "CNY"
                    ],
                    "isTransparent": false,
                    "colorTheme": "light",
                    "locale": "en"
                    }
                    </script>
                    </div>
<!-- TradingView Widget END -->

                <!-- <div class="forecast-text">
                    According to Amazon’s stock trend analysis and forecasts, there’s more than a good chance that the company could bring substantial growth to your portfolio in the next two years.
                </div> -->
                <!-- <button type="button" class="amazon-btn yellow-button btn btn-primary">Discover how to benefit from investing in Amazon</button> -->
            </div>

        </div>
    </div>
    <!--footer-->
    <div class="footer-bg">
        <div class="footer-container container">
            <!-- <a href="#"><img class="footer-logo" src="./image/investingfinancial-logo.png"></a> -->
            <div class="bottom-text">
                <p>HIGH RISK INVESTMENT WARNING: Investments are complex instruments and come with a high risk of losing money rapidly due to leverage. 82% of retail investor accounts lose money when trading Investments with this provider. You should consider whether you understand how Investments work and whether you can afford to take the high risk of losing your money. Click here to read our Risks Disclosure. Phoenix Markets is the trading name of WGM Services Ltd, a financial services company subject to the regulation and supervision of the Cyprus Securities Exchange Commission (CySEC) under license number 203/13. The offices of WGM Services Ltd are located at 11, Vizantiou, 4th Floor, Strovolos 2064, Nicosia, Cyprus.</p>
                <p>The information and services contained on this website are not intended for residents of the United States or Canada.</p>
            </div>
            <div class="footer-link-div">
                <div class="a-div">
                    <a target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/privacy_policy.pdf" class="a-tag">
                        Privacy Policy</a>
                </div>
                <div class="a-div">
                    <a target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/terms_and_conditions.pdf" class="a-tag">
                        Terms and Conditions</a>
                </div>
                <div class="a-div">
                    <a target="_blank" href="https://www.phoenixmarkets.com/doc/regulation/policies/cookies_policy.pdf" class="a-tag">
                        Cookie Policy</a>
                </div>
            </div>

        </div>
    </div>
    <!-- <script src="intl-tel-input-master/intl-tel-input-master/build/js/intlTelInput.js"></script> -->
    <script>
        /* scroll to div */
        $(".amazon-btn").click(function() {
            $('html, body').animate({
                scrollTop: $(".hero-form-bg").offset().top
            }, 2000);
        });
    </script>

<script>
        const phoneInputField = document.querySelector("#phone");
        const phoneInput = window.intlTelInput(phoneInputField, {
            initialCountry: "auto",
            geoIpLookup: function(success) {
                // Get your api-key at https://ipdata.co/
                fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
                    .then(function(response) {
                        if (!response.ok) return success("");
                        return response.json();
                    })
                    .then(function(ipdata) {
                        success(ipdata.country_code);
                    });
            },
            utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
        });

        $('#commence-btn').click(function() {
            var code = phoneInput.getNumber();
            $('#phone').val(code);
            var title = $("title").text();
            $('#title').val(title);
        });
    </script>

    <script>
        var checkName = function() {
            var regExp = new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})");
            var fname = document.getElementById("fname").value;
            if ((fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 2)) {
                document.getElementById("fname-message").innerHTML = "Name should be followed by First name , Space and then Last name.";
                return false;
            }
            if (fname.length > 32) {
                document.getElementById("fname-message").innerHTML = "The length of the name should not exceed 32 characters.";
                return false;
            }
            document.getElementById("fname-message").innerHTML = " ";
            return true;
        }
    </script>

    <script>
        $('#email').first().keyup(function() {
            var $email = this.value;
            validateEmail($email);
        });

        function validateEmail(email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if ((email.length < 2) || (!emailReg.test(email))) {
                document.getElementById("email-message").innerHTML = "This field is required, please enter a valid email address";
            } else {
                document.getElementById("email-message").innerHTML = " ";
                return true;
            }
        }
    </script>

    <script>
        $('#phone').first().keyup(function() {
            var phone = this.value;
            if (phone.length < 5) {
                document.getElementById("phone-message").innerHTML = "This field is required";
            } else {
                document.getElementById("phone-message").innerHTML = " ";
                return true;
            }
        });
    </script>

    <script>
        var checkPassword = function() {
            var pw = document.getElementById("password").value;
            if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
                document.getElementById("password-message").innerHTML = "The password length must be at least 8 characters. The password must contain at least one letter, one capital letter and one number";
                return false;
            }
            if (pw.length > 15) {
                document.getElementById("password-message").innerHTML = "The password length must not exceed 15 characters.";
                return false;
            }
            document.getElementById("password-message").innerHTML = " ";
            return true;
        }
    </script>

    <script>
        function Validation() {
            var regExp = new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})");
            var fname = document.getElementById("fname").value;
            if ((fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 2)) {
                document.getElementById("fname-message").innerHTML = "Please enter only your first and last name";
                return false;
            }
            if (fname.length > 32) {
                document.getElementById("fname-message").innerHTML = "The length of the name must not exceed 32 characters.";
                return false;
            }

            var pw = document.getElementById("password").value;
            if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
                document.getElementById("message").innerHTML = "Password length must be at least 8 characters. Password must contain at least one letter, one uppercase and one number";
                return false;
            }
            if (pw.length > 15) {
                document.getElementById("message").innerHTML = "Password Length should not be more than 15 Characters.";
                return false;
            }

            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var emails = document.getElementById("email").value;
            if ((emails.length < 2) || (!emailReg.test(emails))) {
                document.getElementById("email-message").innerHTML = "This field is required, please enter a valid email address";
                return false;
            }

            var phone = document.getElementById("phone").value;
            if (phone.length < 5) {
                document.getElementById("phone-message").innerHTML = "This field is required.";
                return false;
            }
        }
    </script>
</body>

</html>